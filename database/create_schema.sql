CREATE SCHEMA poodle_e_learning;

CREATE TABLE poodle_e_learning.tokens ( 
	token                varchar(600)  NOT NULL    PRIMARY KEY
 );

CREATE TABLE poodle_e_learning.topics ( 
	topic_id             int  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	topic_name           varchar(100)  NOT NULL    
 );

CREATE TABLE poodle_e_learning.users ( 
	user_id              int  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	email                varchar(100)  NOT NULL    ,
	first_name           varchar(20)  NOT NULL    ,
	last_name            varchar(30)  NOT NULL    ,
	birthdate            date  NOT NULL    ,
	password             varchar(100)  NOT NULL    ,
	registration_date    datetime  NOT NULL DEFAULT current_timestamp   ,
	role                 varchar(10)  NOT NULL DEFAULT 'student'   ,
	user_isdeactivated   tinyint  NOT NULL DEFAULT 0   ,
	CONSTRAINT unq_users UNIQUE ( email ) 
 );

CREATE TABLE poodle_e_learning.courses ( 
	course_id            int  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	course_title         varchar(100)  NOT NULL    ,
	course_description   longtext  NOT NULL    ,
	course_isprivate     tinyint  NOT NULL DEFAULT 0   ,
	course_upload_date   datetime  NOT NULL DEFAULT current_timestamp   ,
	course_date_restriction datetime      ,
	course_isdeleted     tinyint  NOT NULL DEFAULT 0   ,
	course_owner_id      int  NOT NULL    ,
	course_last_update   datetime      ,
	CONSTRAINT fk_courses_users FOREIGN KEY ( course_owner_id ) REFERENCES poodle_e_learning.users( user_id ) ON DELETE NO ACTION ON UPDATE CASCADE
 );

CREATE INDEX fk_courses_users ON poodle_e_learning.courses ( course_owner_id );

CREATE TABLE poodle_e_learning.sections ( 
	section_id           int  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	section_title        varchar(100)  NOT NULL    ,
	section_description  longtext  NOT NULL    ,
	section_upload_date  datetime  NOT NULL DEFAULT current_timestamp   ,
	section_date_restriction datetime      ,
	section_isdeleted    tinyint  NOT NULL DEFAULT 0   ,
	section_course_id    int  NOT NULL    ,
	section_last_update  datetime      ,
	section_order        int  NOT NULL    ,
	CONSTRAINT fk_sections_courses FOREIGN KEY ( section_course_id ) REFERENCES poodle_e_learning.courses( course_id ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX fk_sections_courses ON poodle_e_learning.sections ( section_course_id );

CREATE TABLE poodle_e_learning.topics_courses ( 
	topic_id             int  NOT NULL    ,
	course_id            int  NOT NULL    ,
	CONSTRAINT pk_topics_courses PRIMARY KEY ( topic_id, course_id ),
	CONSTRAINT fk_topics_courses_courses FOREIGN KEY ( course_id ) REFERENCES poodle_e_learning.courses( course_id ) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_topics_courses_topic FOREIGN KEY ( topic_id ) REFERENCES poodle_e_learning.topics( topic_id ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX fk_topics_courses_course ON poodle_e_learning.topics_courses ( course_id );

CREATE TABLE poodle_e_learning.announces ( 
	announce_id          int  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	announce_content     longtext  NOT NULL    ,
	announce_uploaded_date datetime   DEFAULT current_timestamp   ,
	announce_course_id   int  NOT NULL    ,
	announce_title       varchar(50)  NOT NULL    ,
	CONSTRAINT fk_announces_files FOREIGN KEY ( announce_course_id ) REFERENCES poodle_e_learning.courses( course_id ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX fk_announces_files ON poodle_e_learning.announces ( announce_course_id );

CREATE TABLE poodle_e_learning.course_allowed_users ( 
	user_id              int  NOT NULL    ,
	course_id            int  NOT NULL    ,
	CONSTRAINT pk_course_allowed_users PRIMARY KEY ( user_id, course_id ),
	CONSTRAINT fk_course_allowed_users FOREIGN KEY ( course_id ) REFERENCES poodle_e_learning.courses( course_id ) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_course_allowed_users_user FOREIGN KEY ( user_id ) REFERENCES poodle_e_learning.users( user_id ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX fk_course_allowed_users_course ON poodle_e_learning.course_allowed_users ( course_id );

CREATE TABLE poodle_e_learning.lessons ( 
	lesson_id            int  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	lesson_title         varchar(100)  NOT NULL    ,
	lesson_description   longtext  NOT NULL    ,
	lesson_upload_date   datetime  NOT NULL DEFAULT current_timestamp   ,
	lesson_isdeleted     tinyint  NOT NULL DEFAULT 0   ,
	lesson_section_id    int  NOT NULL    ,
	lesson_date          datetime      ,
	lesson_last_update   datetime      ,
	lesson_order         tinyint  NOT NULL    ,
	lesson_content       longtext  NOT NULL    ,
	CONSTRAINT fk_lessons_courses FOREIGN KEY ( lesson_section_id ) REFERENCES poodle_e_learning.sections( section_id ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX fk_lessons_courses ON poodle_e_learning.lessons ( lesson_section_id );

CREATE TABLE poodle_e_learning.lesson_stats ( 
	user_id              int  NOT NULL    ,
	lesson_id            int  NOT NULL    ,
	CONSTRAINT pk_lesson_stats PRIMARY KEY ( user_id, lesson_id ),
	CONSTRAINT fk_lesson_stats_lessons FOREIGN KEY ( lesson_id ) REFERENCES poodle_e_learning.lessons( lesson_id ) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_lesson_stats_users FOREIGN KEY ( user_id ) REFERENCES poodle_e_learning.users( user_id ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX fk_lesson_stats_lessons ON poodle_e_learning.lesson_stats ( lesson_id );

