import fs from 'fs';
import bcrypt from 'bcrypt';

const data = JSON.parse(fs.readFileSync('./manyUsersClearPassword.json'));

Promise.all(
  data.map(async (user) => {
    const {password} = user;

    const cryptedPass = await bcrypt.hash(password, 10);

    return {
      ...user,
      password: cryptedPass,
    };
  }),
).then((data) => {
  
  fs.writeFileSync('./manyUsersHashedPassword.json', JSON.stringify(data));

});

