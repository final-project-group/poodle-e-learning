SELECT 
	c.course_id as courseId,
    c.course_title as courseTitle,
	tc.course_topics as courseTopics,
    c.course_isprivate as courseIsprivate,
    c.course_Description as courseDescription,
    c.course_upload_date as courseUploadDate,
    s.course_sections_info as courseSectionsInfo,
    c.course_date_restriction as courseDateRestriction,
	c.course_last_update as courseLastUpdate,
    c.course_history as courseHistory,
	c.course_owner_id as courseOwnerId,
	u.first_name as ownerFirstName,
	u.last_name as ownerLastName
    
FROM courses c
LEFT JOIN (
	SELECT course_id,     CONCAT('[', group_CONCAT(
		CONCAT('{"topicId":', t.topic_id, 
				', "topicName":"', t.topic_name,
				'"}', ']')))
	as course_topics
	FROM topics_courses tc
	JOIN topics t
		ON tc.topic_id = t.topic_id
	WHERE tc.course_id = 1
	GROUP BY tc.course_id) tc
	ON tc.course_id = c.course_id
LEFT JOIN (
	SELECT
		c.course_id as course_id,
		CONCAT('[', group_CONCAT(
		CONCAT('{"sectionId":', s.section_id, 
				', "sectionTitle":"', s.section_title, 
				'", "sectionDescription":"', s.section_description, 
				'", "sectionUploadDate":"', s.section_upload_date, 
				'", "sectionDateRestriction":"', COALESCE(s.section_date_restriction, ''), 
				'", "sectionOrder":"', s.section_order, 
				'"}'))
				, ']') as course_sections_info
	FROM courses c
	LEFT JOIN sections s
		ON c.course_id = s.section_course_id
	WHERE c.course_id = 1 AND s.section_isdeleted = 0    ) s
	ON s.course_id = c.course_id
LEFT JOIN users u
	ON u.user_id = c.course_owner_id
WHERE c.course_id = 1 AND c.course_isdeleted = 0
GROUP BY c.course_id