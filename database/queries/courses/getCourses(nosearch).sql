    SELECT * FROM (
      SELECT c.course_id,
        c.course_title,
        c.course_description,
        c.course_isprivate,
        c.course_upload_date,
        c.course_date_restriction,
        c.course_owner_id,
        u.first_name,
        u.last_name,
          Group_CONCAT(tc.topic_id SEPARATOR ',') as topic_ids,
          Group_CONCAT(tc.topic_name SEPARATOR ',') as topic_names
          FROM courses c
      LEFT JOIN (
        SELECT t.topic_id, t.topic_name, c.course_id
          FROM topics t
          JOIN topics_courses c
          ON t.topic_id = c.topic_id
          ) as tc
        ON tc.course_id = c.course_id
      LEFT JOIN users u
        ON c.course_owner_id = u.user_id
      WHERE c.course_isdeleted = 0
     
      GROUP BY c.course_id
      ORDER BY c.course_upload_date DESC) r
  