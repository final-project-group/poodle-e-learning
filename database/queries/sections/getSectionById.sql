SELECT 
	s.section_id,
	s.section_title,
	s.section_description,
	s.section_upload_date,
	l.section_lessons_info,
	s.section_date_restriction,
	s.section_last_update,
	s.section_history,
	c.course_owner_id as section_owner_id,
	u.first_name as section_owner_first_name,
	u.last_name as section_owner_last_name
FROM sections s
LEFT JOIN (
	SELECT
		s.section_id as section_id,
		CONCAT('[', group_CONCAT(
		CONCAT('{"lessonId":', l.lesson_id, 
			', "lessonTitle":"', l.lesson_title, 
			'", "lessonDescription":"', l.lesson_description, 
			'", "lessonUploadDate":"', l.lesson_upload_date, 
			'", "lessonOrder":"', l.lesson_order, 
			'", "lessonDate":"', COALESCE(l.lesson_order, ''), 
			'"}') ORDER BY l.lesson_order)
			, ']') as section_lessons_info
	FROM sections s
	LEFT JOIN lessons l
		ON s.section_id = l.lesson_section_id
	WHERE s.section_id = 1 AND l.lesson_isdeleted = 0) l
	ON l.section_id = s.section_id
LEFT JOIN courses c
	ON s.section_course_id = c.course_id
LEFT JOIN users u
 	ON u.user_id = c.course_owner_id
WHERE s.section_id = 1 AND s.section_isdeleted = 0
GROUP BY s.section_id